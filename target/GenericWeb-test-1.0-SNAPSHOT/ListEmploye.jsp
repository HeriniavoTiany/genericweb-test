<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Employés</title>
</head>
<body>
<h1>Informations reçus: </h1>
<p>Name:
    <b><%= request.getAttribute("name") %></b>
</p>
<p>
    Age:
    <b><%= request.getAttribute("age") %></b>
</p>
<a href="emp-add.do">
    <button>Retour</button>
</a>
</body>
</html>
