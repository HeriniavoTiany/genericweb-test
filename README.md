# GenericWeb Test

## pages de tests:
- Sans paramètres: 
  - `/emp-add.do`: page par défaut. Contient une formulaire pour rentrer un employé. Il affiche également ce qu'il y a dans la session (null la première fois)
- Avec paramètres:
  - `/emp-list.do`: Page qui affiche ce qui vient d'etre envoyé grâce au formulaire de `emp-add.do` et met le nom dans la session.   