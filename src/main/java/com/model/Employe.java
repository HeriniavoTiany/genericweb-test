package com.model;

import genericweb.Scannable;
import genericweb.Session;
import genericweb.URL;
import genericweb.ViewModel;

import java.util.HashMap;

@Scannable
public class Employe {

    String name;
    Integer age;
    String departement;

    @URL(name = "emp-add")
    public ViewModel save() {
        HashMap<String, Object> attribute = new HashMap<>();
        attribute.put("input1", "Name");
        attribute.put("input2", "Age");
        return new ViewModel("AddEmploye.jsp", attribute);
    }

    @URL(name = "emp-list")
    public ViewModel list() {
        HashMap<String, Object> attribute = new HashMap<>();
        attribute.put("name", name);
        attribute.put("age", age);

        Session.setAttribute("name", name);

        return new ViewModel("ListEmploye.jsp", attribute);
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }
}
