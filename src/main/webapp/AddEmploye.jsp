<%@ page import="genericweb.Session" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Employé</title>
</head>
<body>
<p>Session.getAttribute("name") = <%= Session.getAttribute("name") %></p>
<form action="emp-list.do">
    <p>
        <b><%= request.getAttribute("input1") %></b>
        <input type="text" name="name">
    </p>
    <p>
        <b><%= request.getAttribute("input2") %></b>
        <input type="number" name="age">
    </p>
    <input type="submit" value="Ajouter">
</form>
</body>
</html>
